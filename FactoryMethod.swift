protocol CarFactory {
    func makeCar() -> Car
}

protocol Car{
    func getType() -> String
}

class YarisFactory: CarFactory{
    func makeCar() -> Car{
        return Yaris()
    }
}

class Yaris: Car{
    func getType() -> String{
        return "Yaris"
    }
}

func main(){
    let factory = YarisFactory()
    let car = factory.makeCar()
    print(car.getType())
}

main()