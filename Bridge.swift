    //Implementation
protocol OperatingSystem {
    func getInfo() -> String 
    func setInfo(info:String)
    func printStatus()
    func statusBrowser()
}

class OperatingSystemOS: OperatingSystem{
    private var info: String = ""
    private var statusBrowser = false
    func getInfo() -> String{
        return info
    }
    func setInfo(info: String){
        self.info = info
    }
    func printStatus(){
        print("----")
        print("I'm an operating system OS")
        print("Info: \(info)")
    }
}

class OperatingSystemAndroid: OperatingSystem{
    private var info: String = ""
    func getInfo() -> String{
        return info
    }
    func setInfo(info:String){
        self.info = info
    }
    func printStatus(){
        print("----")
        print("I'm an operating system Android")
        print("Info: \(info)")
    }
}

    //Abstraction
protocol browser{
    func open()
    func close()
    func goPage(page:String)
}

class fireFoxBrowser: browser{
    var operatingSystem: OperatingSystem?

    func open(){
        print("Firefox opened on screen")
    }
    func close(){
        print("Firefox closed")
    }
    func goPage(page:String){
        print("The page \(page) opened")
    }
    init(operatingSystem: OperatingSystem){
        self.operatingSystem = operatingSystem
    }
}

func test(operatingSystem: OperatingSystem){
    operatingSystem.setInfo("Test")
    print(operatingSystem.getInfo())
}

func main(){

}