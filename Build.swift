protocol Builder {
    func wheels()
    func doors()
    func color()    
}

class Yaris{
    private var parts = [String]()
    func add(part: String){
        self.parts.append(part)
    }
    func listParts() -> String{
        return "Partes del vehiculo" + parts.joined(separator: ", ") + "\n"
    }
}

class CarBuilder: Builder{
    private var product = Yaris()

    func reset(){
        product = Yaris()
    }

    func wheels(){
        product.add(part: "4")
    }

    func doors(){
        product.add(part: "5")
    }

    func color(){
        product.add(part: "Blanco")
    }

    func retrieveProduct() -> Yaris {
        let result = self.product
        reset()
        return result
    }
}

class Director{
    private var  builder: Builder?

    func update(builder: Builder){
        self.builder = builder
    }

    func buildMinimalViableCar(){
        builder?.wheels()
    }

    func buildFullFeaturedCar() {
        builder?.wheels()
        builder?.doors()
        builder?.color()
    }
}

class Client{
    static func someClientCode(director: Director){
        let builder = CarBuilder()
        director.update(builder: builder)

        print("Standard Car")
        director.buildMinimalViableCar()
        print(builder.retrieveProduct().listParts())

        print("Standard full Car")
        director.buildFullFeaturedCar()
        print(builder.retrieveProduct().listParts())
        
        print("Custom Car:")
        builder.wheels()
        builder.color()
        print(builder.retrieveProduct().listParts())
    }
}

class BuilderConceptual{
    func testBuilderConceptual(){
        let director = Director();
        Client.someClientCode(director: director)
    }
}