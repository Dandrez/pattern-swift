class Foco {
    var idFoco:String
    var sensores = [Sensor]()
    var estado:String
    init(idFoco:String){
        self.idFoco=idFoco
        self.estado="Apagado"
    }
    func suscribirObservador(sensor:Sensor){
        self.sensores.append(sensor)
    }
    // func eliminarObservador(sensor:Sensor){
    //     self.sensor.
    // }
    func notificarObservadores(){
        for sensor in sensores{
            sensor.notificar()
        }
    }
    func getUltimoSuceso()->String{
        return "\(idFoco) : \(estado)"
    }
    func setUltimoSuceso(estado:String){
        self.estado = estado
        notificarObservadores()
    }
}
class Sensor{
    var idSensor:String
    var foco:Foco
    init(idSensor:String, foco:Foco){
        self.idSensor=idSensor
        self.foco=foco
    }
    func notificar(){
        print("\(idSensor) \(foco.getUltimoSuceso())")
    }
}

func main(){
    let foco1 = Foco(idFoco:"F1")
    let sensorPiso = Sensor(idSensor:"t1",foco:foco1)
    foco1.suscribirObservador(sensor:sensorPiso)
    let sensorTecho = Sensor(idSensor:"s1",foco:foco1)
    foco1.suscribirObservador(sensor:sensorTecho)
    foco1.setUltimoSuceso(estado:"Encendido")
    foco1.setUltimoSuceso(estado:"Apagado")   
}

main()