protocol Combo {
    func getPrecio() -> Float
    func getDescripcion() -> String
}

class ComboCompleto: Combo{
    func getPrecio() -> Float{
        return 4.0
    }
    func getDescripcion() -> String{
        return "2 presas + 1 papa frita pequena + 1 bebida (400 ml) + 1 moncaiba"
    }
}

class ComboIdeal: Combo{
    func getPrecio() -> Float{
        return 6.50
    }
    func getDescripcion() -> String{
        return "3 presas + 1 papa frita pequena + 1 bebida (400 ml) + 1 moncaiba"
    }
}

class complemento:Combo {
    private var comboDecorador:Combo
    init(comboDecorador:Combo){
        self.comboDecorador = comboDecorador
    }
    func getPrecio() -> Float {
        return self.comboDecorador.getPrecio()
    }
    func getDescripcion() -> String {
        return self.comboDecorador.getDescripcion()
    }
}

class DecoratorPapasFritas:complemento{
    private var comboDecorador:Combo
    override init(comboDecorador:Combo){
        self.comboDecorador = comboDecorador
        super.init(comboDecorador:comboDecorador)
    }
    override func getPrecio() -> Float {
        return self.comboDecorador.getPrecio()+0.9
    }
    override func getDescripcion() -> String {
        return self.comboDecorador.getDescripcion() + " + porcio de papas fritas medianas"
    }
}

class DecoratorEnsalada: complemento {
    private var comboDecorador:Combo
    override init(comboDecorador:Combo){
        self.comboDecorador = comboDecorador
        super.init(comboDecorador:comboDecorador)
    }
    override func getPrecio() -> Float {
        return self.comboDecorador.getPrecio() + 0.99
    }
    override func getDescripcion() -> String {
        return self.comboDecorador.getDescripcion() + " + ensalada de col med."
    }

}

var completo:Combo = ComboCompleto()
print(completo.getDescripcion())
print(completo.getPrecio())

print("-----")

var comboIdealConPapasFritasExtra:Combo = DecoratorPapasFritas(comboDecorador:ComboIdeal())
print(comboIdealConPapasFritasExtra.getDescripcion())
print(comboIdealConPapasFritasExtra.getPrecio())
