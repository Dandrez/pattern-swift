protocol Car{
    func getType() -> String
}

class Yaris: Car{
    func getType() -> String{
        return "Yaris"
    }
}

class Vitara: Car{
    func getType() -> String{
        return "Vitara"
    }
}

class CarFactory{
    func makeCar(tipo:String)->Car{
        switch tipo {
        case "Yaris":
            return Yaris()
        case "Vitara":
            return Vitara()  
        default:
            print("No existe")
            return Vitara() 
        }
    }
}

func main(){
    let factory = CarFactory()
    let car = factory.makeCar(tipo:"Yaris")
    print(car.getType())
}

main()
